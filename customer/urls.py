from django.urls import path

from customer.views import *

app_name = 'customer'


urlpatterns = [
    path('', Customers.as_view(), name='list'),
    path('edit/<id>/', Update.as_view(), name='edit'),
    path('<id>/', CustomerDetail.as_view(), name='detail'),
]