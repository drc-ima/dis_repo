from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import *

from distributions.models import Customer


class Customers(LoginRequiredMixin, ListView):
    template_name = 'customer/index.html'
    queryset = Customer.objects.all()


class CustomerDetail(LoginRequiredMixin, DetailView):
    template_name = 'customer/details.html'
    queryset = Customer.objects.all()
    pk_url_kwarg = 'id'


class Update(LoginRequiredMixin, RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return reverse_lazy('customer:detail', kwargs={'id': self.kwargs.get('id')})

    def post(self, request, *args, **kwargs):
        full_name = request.POST.get('full_name')
        contact = request.POST.get('contact')

        customer = Customer.objects.get(id=self.kwargs.get('id'))

        customer.full_name = full_name
        customer.contact = contact
        customer.save()

        return super(Update, self).post(self, request, *args, **kwargs)
