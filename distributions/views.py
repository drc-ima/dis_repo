from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.views.generic import *

from distributions.models import Stock, Product
from .forms import product
from django.urls import reverse_lazy


class NewProduct(LoginRequiredMixin, CreateView):
    template_name = 'product/index.html'
    form_class = product.ProductForm
    success_url = reverse_lazy('distribution:stocks')

    def form_valid(self, form):
        valid = super(NewProduct, self).form_valid(form)
        form.instance.created_by = self.request.user
        form.instance.created_at = timezone.now()
        form.instance.quantity = 0

        stock = Stock.objects.create(
            product=form.instance,
            status=2
        )

        stock.save()
        form.save()

        return valid


class Stocks(LoginRequiredMixin, ListView):
    template_name = 'stock/index.html'
    queryset = Stock.objects.all()


class Transaction(LoginRequiredMixin, ListView):
    template_name = 'product/transact.html'

    def get_queryset(self):
        try:
            product_id = self.kwargs.get('product')

            if product_id == 'gen':
                return Product.objects.all()
            else:
                return Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            return Product.objects.all()
