from django.urls import *
from .views import *

app_name = 'distribution'


urlpatterns = [
    path('stock/', Stocks.as_view(), name='stocks'),
    path('product/new/', NewProduct.as_view(), name='product-new'),
    path('transact/<id>/', Transaction.as_view(), name='transact'),
]