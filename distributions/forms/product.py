from django.forms import *
from ..models import *


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ('product_name', 'brand',)
        widgets = {'product_name': TextInput(attrs={'required': True, 'autofocused': True})}
