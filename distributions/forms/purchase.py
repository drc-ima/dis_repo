from django.forms import *
from ..models import *


class PurchaseForm(ModelForm):
    class Meta:
        model = Purchase
        fields = ('product', 'bulk_type', 'quantity_per_bulk', 'quantity', 'per_bulk_amount', 'supplier')
        widgets = {
            'product': Select(attrs={'required': True}),
            'bulk_type': Select(attrs={'required': True}),
            'quantity': NumberInput(attrs={'required': True}),
            'quantity_per_bulk': NumberInput(attrs={'required': True}),
            'per_bulk_amount': NumberInput(attrs={'required': True}),
            # 'total_amount': NumberInput(attrs={'required': True}),
            'supplier': TextInput(attrs={'required': True}),
        }