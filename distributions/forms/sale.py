from django.forms import *
from ..models import *


class SalesForm(ModelForm):
    errors = {
        'quantity_error': 'The quantity entered is more than available in stock'
    }

    class Meta:
        model = Sale
        fields = ('product', 'quantity', 'unit_quantity_price', 'amount', 'customer')
        # exclude = ('id', 'sale_id', 'sale_at', 'recorded_by',)
        widgets = {
            'product': Select(attrs={'required': True}),
            'quantity': NumberInput(attrs={'required': True}),
            'unit_quantity_price': NumberInput(attrs={'required': True}),
            'amount': NumberInput(attrs={'required': True}),
            'customer_name': TextInput(attrs={'required': True}),
            'customer_contact': NumberInput(attrs={'required': True})
        }

    def clean(self):
        quantity = self.cleaned_data.get('quantity')
        product = self.cleaned_data.get('product')
        stocks = Stock.objects.filter(product=product)
        for stock in stocks:
            if quantity > stock.quantity:
                raise ValidationError(
                    self.errors['quantity_error'],
                    code='quantity_error',
                )
        return quantity
