from django.conf import settings
from django.db.models import *
from django.utils import timezone
from utils.random_codes import stock_id
# from purchases.models import Purchase
# from sales.models import Sale


class Product(Model):
    product_name = CharField(max_length=355, blank=True, null=True)
    brand = CharField(max_length=255, blank=True, null=True)
    unit_price = DecimalField(default=0.0, decimal_places=2, max_digits=8, blank=True, null=True)
    purchases = ManyToManyField('Purchase', related_name='product_purchases', blank=True)
    purchases_total = DecimalField(default=0, blank=True, null=True, max_digits=10, decimal_places=2)
    sales_total = DecimalField(default=0, blank=True, null=True, max_digits=10, decimal_places=2)
    revenue = DecimalField(default=0, blank=True, null=True, max_digits=10, decimal_places=2)
    profit_loss = DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    sales = ManyToManyField('Sale', related_name='product_sales', blank=True)
    quantity = IntegerField(default=0, blank=True, null=True)
    created_by = ForeignKey(settings.AUTH_USER_MODEL, related_name='products_by', on_delete=DO_NOTHING, blank=True, null=True)
    updated_by = ForeignKey(settings.AUTH_USER_MODEL, related_name='products_update', on_delete=DO_NOTHING, blank=True,
                            null=True)
    created_at = DateTimeField(default=timezone.now, blank=True, null=True)
    updated_at = DateTimeField(blank=True, null=True)

    def __str__(self):
        return f'{self.product_name}'

    class Meta:
        db_table = 'product'


STOCK_STATUS = {
    (1, 'In Stock'),
    (2, 'Out of Stock')
}


class Stock(Model):
    stock_id = CharField(default=stock_id(), unique=True, max_length=255, blank=True, null=True)
    product = ForeignKey(Product, on_delete=DO_NOTHING, related_name='stock_product', blank=True, null=True)
    purchased = IntegerField(default=0, blank=True, null=True)
    sold = IntegerField(default=0, blank=True, null=True)
    quantity = IntegerField(default=0, blank=True, null=True)
    status = IntegerField(choices=STOCK_STATUS, blank=True, null=True)

    def __str__(self):
        return f'{self.product.product_name}'

    class Meta:
        db_table = 'stock'


class Sale(Model):
    sale_id = CharField(unique=True, default=stock_id(), max_length=255, blank=True, null=True)
    product = ForeignKey(Product, related_name='sale_product', on_delete=DO_NOTHING, blank=True, null=True)
    customer = ForeignKey('Customer', on_delete=DO_NOTHING, blank=True, null=True, related_name='sale_customer')
    quantity = IntegerField(default=0, blank=True, null=True)
    unit_quantity_price = DecimalField(default=0.0, decimal_places=2, max_digits=8, blank=True, null=True)
    amount = DecimalField(default=0.0, decimal_places=2, max_digits=8, blank=True, null=True)
    recorded_by = ForeignKey(settings.AUTH_USER_MODEL, related_name='sale_by', on_delete=DO_NOTHING, blank=True,
                             null=True)
    sale_at = DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.sale_id} - {self.product}'

    class Meta:
        db_table = 'sale'


BULKY_TYPE = {
    ('Sachet', 'Sachet'),
    ('Crate', 'Crate'),
    ('Box', 'Box')
}


class Purchase(Model):
    purchase_id = CharField(max_length=255, default=stock_id(), unique=True, blank=True, null=True)
    product = ForeignKey(Product, related_name='purchase_products', on_delete=SET_NULL, blank=True, null=True)
    purchase_type = CharField(max_length=255, blank=True, null=True)
    bulk_type = CharField(choices=BULKY_TYPE, max_length=100, blank=True, null=True)
    quantity_per_bulk = IntegerField(default=0, blank=True, null=True)
    quantity = IntegerField(default=0, blank=True, null=True)
    per_bulk_amount = DecimalField(default=0.0, decimal_places=2, max_digits=8, blank=True, null=True)
    total_amount = DecimalField(default=0.0, decimal_places=2, max_digits=8, blank=True, null=True)
    supplier = CharField(max_length=255, blank=True, null=True)
    purchased_at = DateTimeField(default=timezone.now, blank=True, null=True)
    recorded_by = ForeignKey(settings.AUTH_USER_MODEL, related_name='purchase_by', on_delete=DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.purchase_id}'

    class Meta:
        db_table = 'purchase'


class Customer(Model):
    full_name = CharField(max_length=255, blank=True, null=True)
    contact = CharField(max_length=255, blank=True, null=True)
    sales = ManyToManyField(Sale, related_name='customer_sales', blank=True)
    created_at = DateTimeField(default=timezone.now)
    created_by = ForeignKey(settings.AUTH_USER_MODEL, related_name='customers', on_delete=DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.full_name}'

    class Meta:
        db_table = 'customer'
