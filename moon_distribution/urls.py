"""moon_distribution URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from users import urls as user_urls
from distributions import urls as dist_urls
from purchases import urls as purch_urls
from users.views import LoginView, LogoutView
from sales import urls as sale_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('users/', include(user_urls, namespace='users')),
    path('dist/', include(dist_urls, namespace='distribution')),
    path('purchases/', include(purch_urls, namespace='purchases')),
    path('sales/', include(sale_urls, namespace='sales')),
    path('customer/', include('customer.urls', namespace='customer')),
]