from random import randrange


def generate_user_id():
    FROM = '0123456789'
    LENGTH = 7
    user_id = ""
    for i in range(LENGTH):
        user_id += FROM[randrange(0, len(FROM))]
    return f'MEDS{user_id}'


def generate_request_id():
    int_set = "1234567890"
    size = 5
    request_id = ""
    for i in range(size):
        request_id += int_set[randrange(0, len(int_set))]
    return request_id


def stock_id():
    charset = "abcdefghijklmnopqrstuvwxyzAMABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    length = 6
    stockie = ""
    for i in range(length):
        stockie += charset[randrange(0, len(charset))]
    return stockie
