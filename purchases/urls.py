from django.urls import *
from .views import *

app_name = 'purchases'


urlpatterns = [
    path('', Purchases.as_view(), name='purchases'),
    path('add/', NewPurchase.as_view(), name='add'),
]