from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import *

from distributions.forms.purchase import PurchaseForm
from distributions.models import *
from django.urls import reverse_lazy


class Purchases(LoginRequiredMixin, ListView):
    template_name = 'purchases/index.html'
    queryset = Purchase.objects.all()


class NewPurchase(LoginRequiredMixin, CreateView):
    template_name = 'purchases/new.html'
    form_class = PurchaseForm
    success_url = reverse_lazy('purchases:purchases')

    def form_valid(self, form):
        valid = super(NewPurchase, self).form_valid(form)

        stock = Stock.objects.get(product=form.instance.product)
        product = Product.objects.get(id=form.instance.product.id)
        stock.quantity += form.instance.quantity
        product.quantity += form.instance.quantity
        product.purchases.add(form.instance)

        stock.purchased += form.instance.quantity
        form.instance.total_amount = form.instance.quantity * form.instance.per_bulk_amount
        product.purchases_total = int(product.purchases_total) + form.instance.total_amount
        product.profit_loss = product.sales_total - product.purchases_total
        form.instance.recorded_by = self.request.user
        form.purchased_at = timezone.now()
        if form.instance.quantity != 0:
            stock.status = 1
        product.save()
        stock.save()
        form.save()
        return valid
