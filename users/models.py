from utils import managers, random_codes
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
# from cloudinary.models import CloudinaryField


TODAY = timezone.now()

SEX = {
    ('Male', 'Male'),
    ('Female', 'Female'),
    ('Other', 'Other')
}

USER_TYPE = {
    ('ASU', 'Admin Super User'),
    ('ADU', 'Admin User'),
}


class User(AbstractBaseUser, PermissionsMixin):
    user_id = models.CharField(default=random_codes.generate_user_id, unique=True, max_length=100)
    username = models.CharField(
        _('username'),

        unique=True,
        max_length=150
    )
    email = models.EmailField(
        _('Email Address'),
        unique=True,
        max_length=255,
        blank=True,
        null=True
    )
    first_name = models.CharField(_('First Name'), max_length=150, blank=True, null=True)
    last_name = models.CharField(_('Last Name'), max_length=150, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    user_type = models.CharField(_('User Type'), max_length=255, choices=USER_TYPE, blank=True, null=True)
    date_joined = models.DateTimeField(_('Date Joined'), default=timezone.now)
    login_number = models.IntegerField(_('Number of Login'), default=0, blank=True, null=True)
    is_log_in = models.BooleanField(_('Login Status'), blank=True, null=True)
    login_at = models.DateTimeField(blank=True, null=True)

    objects = managers.UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        full_name = f'{self.first_name} {self.last_name}'
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    class Meta:
        ordering = ('user_id', 'first_name', )
        verbose_name_plural = _('Users')
        verbose_name = _('user')

    def __str__(self):
        return f'{self.user_id}, {self.get_full_name()}'
