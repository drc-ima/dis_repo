from django.urls import *
from .views import *

app_name = 'users'

urlpatterns = [
    path('dashboard/', Dashboard.as_view(), name='dashboard'),
]