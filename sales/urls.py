from django.urls import *
from .views import *

app_name = 'sales'


urlpatterns = [
    path('', Sales.as_view(), name='sales'),
    path('add/', NewSalesTemp.as_view(), name='add'),
    # path('add/', NewSale.as_view(), name='add'),
    path('annex/add/', NewSaleRedirect.as_view(), name='add-annex'),
    path('export/<type>/', export, name='export'),
    path('customer/', NewCustomer.as_view(), name='customer'),
]