from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.utils import timezone
from django.views.generic import *
from django.urls import *
from distributions.forms.sale import *
from distributions.models import Sale, Stock, Product
import csv, xlwt


class Sales(LoginRequiredMixin, ListView):
    template_name = 'sales/index.html'
    queryset = Sale.objects.all()


class NewSalesTemp(LoginRequiredMixin, TemplateView):
    template_name = 'sales/new.html'

    def get_context_data(self, **kwargs):
        context = super(NewSalesTemp, self).get_context_data(**kwargs)
        context['products'] = Product.objects.all()
        context['customers'] = Customer.objects.all()
        return context

# class NewSale(LoginRequiredMixin, CreateView):
#     template_name = 'sales/new.html'
#     form_class = SalesForm
#     success_url = '/sales/'
#
#     def form_valid(self, form):
#         valid = super(NewSale, self).form_valid(form)
#         form.instance.recorded_by = self.request.user
#         # form.instance.sale_at = timezone.now()
#         stock = Stock.objects.get(product_id=form.instance.product.id)
#         product = Product.objects.get(product_id=form.instance.product.id)
#         stock.sold += form.instance.quantity
#         stock.quantity = stock.quantity - form.instance.quantity
#         product.sales.add(form.instance)
#         product.quantity = stock.quantity - form.instance.quantity
#
#         product.save()
#         stock.save()
#         form.save()
#         return valid
    
    
class NewSaleRedirect(LoginRequiredMixin, RedirectView):
    url = None
    
    def post(self, request, *args, **kwargs):
        product_id = request.POST.get('product')
        customer = request.POST.get('customer')
        # customer_contact = request.POST.get('customer_contact')
        quantity = request.POST.get('quantity')
        unit_quantity_price = request.POST.get('unit_quantity_price')
        # amount = request.POST.get('amount')

        stock = Stock.objects.get(product_id=product_id)
        product = Product.objects.get(id=product_id)

        if not int(quantity) > stock.quantity:
            amount = int(quantity) * int(unit_quantity_price)
            sale = Sale.objects.create(
                product_id=product_id,
                customer_id=customer,
                # customer_name=customer_name,
                quantity=quantity,
                unit_quantity_price=unit_quantity_price,
                amount=amount,
                recorded_by=self.request.user
            )
            stock.sold += int(sale.quantity)
            stock.quantity = stock.quantity - int(sale.quantity)
            if stock.quantity == 0:
                stock.status = 2
            product.sales.add(sale)
            custome = Customer.objects.get(id=customer)
            custome.sales.add(sale)
            product.sales_total += sale.amount
            product.profit_loss = product.sales_total - product.purchases_total
            product.revenue = product.revenue + sale.amount
            product.save()
            stock.save()
            sale.save()
            self.url = reverse_lazy('sales:sales')
            return super(NewSaleRedirect, self).post(self, request, *args, **kwargs)
        else:
            messages.error(request, 'Quantity is more than stock of product')
            return HttpResponseRedirect(reverse_lazy('sales:add'))


class NewCustomer(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('sales:add')

    def post(self, request, *args, **kwargs):
        full_name = request.POST.get('full_name')
        contact = request.POST.get('contact')

        customer = Customer.objects.create(
            full_name=full_name,
            contact=contact,
            created_by=self.request.user,
        )
        customer.save()

        return super(NewCustomer, self).post(self, request, *args, **kwargs)

# class NewSaleAnnex(LoginRequiredMixin, CreateView):
#     template_name = 'sales/new.html'
#     form_class = SalesForm
#     success_url = reverse_lazy('purchases:purchases')
#
#     # def form_valid(self, form):
#     #     valid = super(NewSaleAnnex, self).form_valid(form)
#     #     form.instance.recorded_by = self.request.user
#     #     # form.instance.sale_at = timezone.now()
#     #     stock = Stock.objects.get(product_id=form.instance.product.id)
#     #     product = Product.objects.get(product_id=form.instance.product.id)
#     #     stock.sold += form.instance.quantity
#     #     stock.quantity = stock.quantity - form.instance.quantity
#     #     product.sales.add(form.instance)
#     #     product.quantity = stock.quantity - form.instance.quantity
#     #
#     #     product.save()
#     #     stock.save()
#     #     form.save()
#     #     return valid


def export(request, **kwargs):
    type = kwargs.get('type')

    if type == 'csv':
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="sales.csv"'

        writer = csv.writer(response)
        writer.writerow(['Sale ID', 'Product', 'Customer Name', 'Customer Contact', 'Quantity', 'Unit Price', 'Total Amount', 'Recorded Date', 'Recorded By'])

        sales = Sale.objects.all()
        for sale in sales:
            writer.writerow([sale.sale_id, sale.product.product_name, sale.customer.full_name, sale.customer.contact, sale.quantity, sale.unit_quantity_price, sale.amount, sale.sale_at, sale.recorded_by.get_full_name()])
        return response
    elif type == 'xls':
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="sales.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Sales')
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['Sale ID', 'Product', 'Customer Name', 'Customer Contact', 'Quantity', 'Unit Price', 'Total Amount', 'Recorded Date', 'Recorded By']

        for col in range(len(columns)):
            ws.write(row_num, col, columns[col], font_style)

        font_style = xlwt.XFStyle()

        rows = Sale.objects.all()
        for row in rows:
            row_num += 1
            selected = [row.sale_id, row.product.product_name, row.customer.full_name, row.customer.contact, row.quantity, row.unit_quantity_price, row.amount, f'{row.sale_at.date()} {row.sale_at.time()}', row.recorded_by.get_full_name()]
            for col in range(len(selected)):
                ws.write(row_num, col, selected[col], font_style)

        wb.save(response)
        return response
